package ulog

import (
	"io"
	"log"
	"runtime"
)

var (
	TraceLog   *log.Logger
	InfoLog    *log.Logger
	WarningLog *log.Logger
	ErrorLog   *log.Logger
)

func LogInit(
	traceHandle io.Writer,
	infoHandle io.Writer,
	warningHandle io.Writer,
	errorHandle io.Writer) {

	TraceLog = log.New(traceHandle,
		"TRACE: ",
		log.Ldate|log.Ltime|log.Lshortfile)

	InfoLog = log.New(infoHandle,
		"INFO: ",
		log.Ldate|log.Ltime|log.Lshortfile)

	WarningLog = log.New(warningHandle,
		"WARNING: ",
		log.Ldate|log.Ltime|log.Lshortfile)

	ErrorLog = log.New(errorHandle,
		"ERROR: ",
		log.Ldate|log.Ltime|log.Lshortfile)
}

func Error(st string) {
	ErrorLog.Printf(st)
	trace := make([]byte, 1024)
	_ = runtime.Stack(trace, false)
	ErrorLog.Printf("----------")
	ErrorLog.Printf("%s", trace)
	ErrorLog.Printf("----------")
}

func Errorf(format string, err interface{}) {
	ErrorLog.Printf(format, err)
}

func Info(st string) {
	InfoLog.Printf(st)
}

func Warning(st string) {
	WarningLog.Printf(st)
}

func Trace(st string) {
	TraceLog.Printf(st)
}
