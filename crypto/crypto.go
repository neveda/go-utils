package crypto

import (
	"crypto/rand"

	"bitbucket.org/pbgc/go-utils/ulog"
)

func NewSalt(n int) []byte {
	b := make([]byte, n)
	if _, err := rand.Read(b); err != nil {
		ulog.Error(err.Error())
		panic(err.Error())
	}
	return b
}
