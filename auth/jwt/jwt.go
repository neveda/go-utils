package jwt

import (
	"errors"
	"net/http"
	"time"

	auth_crypto "bitbucket.org/pbgc/go-utils/crypto"
	"bitbucket.org/pbgc/go-utils/ulog"
	jwt "github.com/dgrijalva/jwt-go"
	jwt_request "github.com/dgrijalva/jwt-go/request"
)

var (
	ErrTokenExpired    = errors.New("Token Expired, get a new one")
	ErrTokenValidation = errors.New("JWT Token ValidationError")
	ErrTokenParse      = errors.New("JWT Token Error Parsing the token or empty token")
	ErrTokenInvalid    = errors.New("JWT Token is not Valid")

	logOn = true
)

type Options struct {
	SigningMethod  string
	PublicKey      string
	PrivateKey     string
	Expiration     time.Duration
	LongExpiration time.Duration
}

// Generates a JSON Web Token given an userId (typically an id or an email), and the JWT options
// to set SigningMethod and the keys you can check
// http://github.com/dgrijalva/jwt-go
//
// In case you use an symmetric-key algorithm set PublicKey and PrivateKey equal to the SecretKey ,
func (userId string, op Options, long bool) (string, error) {
	now := time.Now()
	var exp int64
	if long {
		exp = now.Add(op.LongExpiration).Unix()
	} else {
		exp = now.Add(op.Expiration).Unix()
	}
	// Create the Claims
	claims := &jwt.StandardClaims{
		IssuedAt:  now.Unix(),
		ExpiresAt: exp,
		Subject:   userId,
		Id:        string(auth_crypto.NewSalt(32)[:]),
	}
	token := jwt.NewWithClaims(jwt.GetSigningMethod(op.SigningMethod), claims)
	privateKey, err := jwt.ParseRSAPrivateKeyFromPEM([]byte(op.PrivateKey))
	if err != nil {
		ulog.Errorf("ERROR: Parsing Private Key: %v\n", err)
		return "", err
	}
	tokenString, err := token.SignedString(privateKey)
	if err != nil {
		ulog.Errorf("ERROR: GenerateJWTToken: %v\n", err)
		return "", err
	}
	return tokenString, err
}

// Validates the token that is passed in the request with the Authorization header
// Authorization: Bearer eyJhbGciOiJub25lIn0
//
// Returns the userId, token (base64 encoded), error
func ValidateToken(r *http.Request, publicKey string) (string, string, error) {
	token, err := jwt_request.ParseFromRequest(r, jwt_request.AuthorizationHeaderExtractor, func(token *jwt.Token) (interface{}, error) {
		return jwt.ParseRSAPublicKeyFromPEM([]byte(publicKey))
	})

	if err != nil {
		switch err.(type) {
		case *jwt.ValidationError:
			vErr := err.(*jwt.ValidationError)
			switch vErr.Errors {
			case jwt.ValidationErrorExpired:
				ulog.Errorf("ERROR: JWT Token Expired: %+v\n", vErr.Errors)
				return "", "", ErrTokenExpired
			default:
				ulog.Errorf("ERROR: JWT Token ValidationError: %+v\n", vErr.Errors)
				return "", "", ErrTokenValidation
			}
		}
		return "", "", ErrTokenParse
	}

	if !token.Valid {
		return "", "", ErrTokenInvalid
	}

	// otherwise is a valid token
	claims := token.Claims.(jwt.MapClaims)
	userId := claims["sub"].(string)

	return userId, token.Raw, nil
}
