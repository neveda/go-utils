package jwt

import (
	"context"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"
	"time"

	auth_user "bitbucket.org/pbgc/go-utils/auth"
	"bitbucket.org/pbgc/nctx"
	"bitbucket.org/pbgc/go-utils/ujson"
)

const (
	TokenKey = "token"
	UserKey  = "user_id"
)

type AuthRoute struct {
	// userStore store.UserRepository
	options Options
}

func NewAuthRoute(signingMethod string, publicKey string, privateKey string, expiration time.Duration, longExpiration time.Duration) *AuthRoute {

	puk, err := ioutil.ReadFile(publicKey)
	if err != nil {
		panic(err.Error())
	}
	prk, err := ioutil.ReadFile(privateKey)
	if err != nil {
		panic(err.Error())
	}
	options := Options{
		SigningMethod:  signingMethod,
		PrivateKey:     string(prk),
		PublicKey:      string(puk),
		Expiration:     expiration * time.Minute,
		LongExpiration: longExpiration * time.Minute,
	}
	return &AuthRoute{
		options: options,
	}
}

func (a *AuthRoute) Login(ctx context.Context, w http.ResponseWriter, r *http.Request) {
	var authForm map[string]string
	err := ujson.RequestToJSON(r, &authForm)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	username, ok := authForm["username"]
	if !ok {
		http.Error(w, "Invalid Username or Password", http.StatusUnauthorized)
		return
	}
	pass, ok := authForm["password"]
	if !ok {
		http.Error(w, "Invalid Username or Password", http.StatusUnauthorized)
		return
	}
	user, err := auth_user.Login(ctx, username, pass)
	if err != nil {
		http.Error(w, "Invalid Username or Password", http.StatusUnauthorized)
		return
	}
	rem := false
	rem_st, ok := authForm["remember"]
	expiresIn := a.options.Expiration.Seconds()
	if ok {
		if rem_st == "true" {
			rem = true
			expiresIn = a.options.LongExpiration.Seconds()
		}
	}
	token, err := GenerateJWTToken(strconv.Itoa(user.Id), a.options, rem)
	if err != nil {
		http.Error(w, "Error while Signing Token", http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	fmt.Fprint(w, ujson.JSONMap{"token": token, "expiresIn": expiresIn})
}

func (a *AuthRoute) RefreshToken(ctx context.Context, w http.ResponseWriter, r *http.Request) {
	userId, _, err := a.authenticate(w, r)
	if err != nil {
		http.Error(w, err.Error(), http.StatusUnauthorized)
		return
	}

	token, err := GenerateJWTToken(userId, a.options, false)
	if err != nil {
		http.Error(w, "Error while Signing Token", http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	fmt.Fprint(w, ujson.JSONMap{"token": token, "expiresIn": a.options.Expiration.Seconds()})
}

func (a *AuthRoute) authenticate(w http.ResponseWriter, r *http.Request) (string, string, error) {
	auth := r.Header.Get("Authorization")
	if auth == "" {
		return "", "", errors.New("Error no token is provided")
	}
	userId, token, err := ValidateToken(r, a.options.PublicKey)
	if err != nil {
		return "", "", err
	}
	return userId, token, nil
}

// Auth handler for nctx implementation
func (a *AuthRoute) AuthContextHandler(ctx context.Context, handler nctx.ContextHandlerFunc) *nctx.ContextAdapter {

	return &nctx.ContextAdapter{
		Ctx: ctx,
		Handler: nctx.ContextHandlerFunc(func(ctx context.Context, w http.ResponseWriter, r *http.Request) {
			userId, token, err := a.authenticate(w, r)
			if err != nil {
				http.Error(w, err.Error(), http.StatusUnauthorized)
				return
			}
			ctx = context.WithValue(ctx, TokenKey, token)
			ctx = context.WithValue(ctx, UserKey, userId)
			_, cancel := context.WithCancel(ctx)
			defer cancel()
			handler.ServeHTTPContext(ctx, w, r)
		}),
	}
}

// // Auth middleware for negroni
// func (a *AuthRoute) AuthMiddleware(w http.ResponseWriter, r *http.Request, next http.HandlerFunc) {
// 	userId, token, err := a.authenticate(w, r)

// 	if err != nil {
// 		http.Error(w, err.Error(), http.StatusUnauthorized)
// 		return
// 	}

// 	context.Set(r, TokenKey, token)
// 	context.Set(r, UserKey, userId)
// 	next(w, r)
// 	context.Clear(r)

// }

// Auth Handler for net/http
// func (a *AuthRoute) AuthHandler(h http.Handler) http.Handler {
// 	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
// 		userId, token, err := a.authenticate(w, r)
// 		if err != nil {
// 			http.Error(w, err.Error(), http.StatusUnauthorized)
// 			return
// 		}
// 		context.Set(r, TokenKey, token)
// 		context.Set(r, UserKey, userId)
// 		h.ServeHTTP(w, r)
// 		context.Clear(r)
// 	})
// }

// func (a *AuthRoute) AuthHandlerFunc(next http.HandlerFunc) http.Handler {
// 	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
// 		a.AuthMiddleware(w, r, next)
// 	})
// }
