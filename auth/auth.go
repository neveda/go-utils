package auth

import (
	"context"
	"crypto/sha256"
	"crypto/subtle"
	"database/sql"
	"encoding/base64"
	"errors"
	"fmt"
	"strconv"
	"strings"

	"golang.org/x/crypto/pbkdf2"

	"github.com/jmoiron/sqlx"
	"github.com/lib/pq"

	auth_crypto "bitbucket.org/pbgc/go-utils/crypto"
	"bitbucket.org/pbgc/go-utils/ulog"
)

var (
	ErrUserNotFound  = errors.New("User not found")
	ErrWrongPassword = errors.New("Email or Password is incorrect")
)

type User struct {
	Id           int
	Username     string
	First_name   string
	Last_name    string
	Email        string
	Password     string `json:"-"`
	Is_active    bool
	Is_staff     bool
	Is_superuser bool
	Last_login   pq.NullTime
	Date_joined  pq.NullTime
}

func (user *User) FirstAndLastName() string {
	return user.First_name + " " + user.Last_name
}

func MakePassword(password string) (string, error) {
	algorithm := "pbkdf2_sha256"
	iterations := 15000
	salt := auth_crypto.NewSalt(12)
	dk := pbkdf2.Key([]byte(password), []byte(salt), iterations, 32, sha256.New)
	encoded := base64.StdEncoding.EncodeToString(dk)
	return fmt.Sprintf("%s$%d$%s$%s", algorithm, iterations, salt, encoded), nil
}

func (user *User) CheckPassword(password string) (bool, error) {
	s := strings.Split(user.Password, "$")
	_, sit, salt, hsh := s[0], s[1], s[2], s[3]
	it, err := strconv.Atoi(sit)
	if err != nil {
		return false, err
	}
	dk := pbkdf2.Key([]byte(password), []byte(salt), it, 32, sha256.New)
	encoded := base64.StdEncoding.EncodeToString(dk)
	return subtle.ConstantTimeEq(int32(len(hsh)), int32(len(encoded))) == 1 &&
		subtle.ConstantTimeCompare([]byte(hsh), []byte(encoded)) == 1, nil
}

func UserList(ctx context.Context) ([]User, error) {
	db := ctx.Value("DB").(*sqlx.DB)
	user_list := []User{}
	if err := db.Select(&user_list, `SELECT * FROM "auth_user" ORDER BY id`); err != nil && err != sql.ErrNoRows {
		return nil, err
	}
	return user_list, nil
}

func UserById(ctx context.Context, id int) (*User, error) {
	db := ctx.Value("DB").(*sqlx.DB)
	U := User{}
	if err := db.Get(&U, `SELECT * FROM "auth_user" WHERE id=$1`, id); err != nil {
		if err == sql.ErrNoRows {
			return nil, ErrUserNotFound
		}
		return nil, err
	}
	return &U, nil
}

func UserByUsername(ctx context.Context, username string) (*User, error) {
	db := ctx.Value("DB").(*sqlx.DB)
	U := User{}
	if err := db.Get(&U, `SELECT * FROM "auth_user" WHERE username=$1`, username); err != nil {
		if err == sql.ErrNoRows {
			return nil, ErrUserNotFound
		}
		return nil, err
	}
	return &U, nil
}

func UserByEmail(ctx context.Context, email string) (*User, error) {
	db := ctx.Value("DB").(*sqlx.DB)
	U := User{}
	if err := db.Get(&U, `SELECT * FROM "auth_user" WHERE email=$1`, email); err != nil {
		if err == sql.ErrNoRows {
			return nil, ErrUserNotFound
		}
		return nil, err
	}
	return &U, nil
}

func Login(ctx context.Context, username, pass string) (*User, error) {
	U, err := UserByUsername(ctx, username)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, ErrUserNotFound
		}
		ulog.Error(err.Error())
		return nil, err
	}
	ok, err := U.CheckPassword(pass)
	if err != nil {
		return nil, err
	}
	if !ok {
		return nil, ErrWrongPassword
	}
	return U, nil
}
