package uconv

import "strconv"

func SliceAtoi(sa []string) ([]int, error) {
	if len(sa) == 0 {
		return []int{}, nil
	}
	si := make([]int, 0, len(sa))
	for _, a := range sa {
		i, err := strconv.Atoi(a)
		if err != nil {
			return si, err
		}
		si = append(si, i)
	}
	return si, nil
}

func SliceItoa(sa []int) []string {
	if len(sa) == 0 {
		return []string{}
	}
	si := make([]string, 0, len(sa))
	for _, i := range sa {
		si = append(si, strconv.Itoa(i))
	}
	return si
}

func SliceIftoi(a []interface{}) []int {
	var b []int
	for _, n := range a {
		b = append(b, int(n.(float64)))
	}
	return b
}
