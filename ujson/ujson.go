package ujson

import (
	"encoding/json"
	"fmt"
	"net/http"
	"reflect"
	"strconv"

	"bitbucket.org/pbgc/go-utils/ustrings"
)

type JSONMap map[string]interface{}

func (j JSONMap) String() (s string) {
	b, err := json.Marshal(j)
	if err != nil {
		return ""
	}
	return string(b)
}

/*
	if err := json.Unmarshal(result, &persTemp); err != nil {
		return nil, err
	}
*/

func (j JSONMap) Validate(structVal interface{}) (bool, []string) {
	// st := reflect.TypeOf(structVal).Elem()  // var st reflect.Type ... e passava R((*S)(nil))
	sv := reflect.ValueOf(structVal)
	probs := make([]string, 0, sv.NumField())
	for i := 0; i < sv.NumField(); i++ {
		// field := sv.Field(i)
		// field_name := strings.ToLower(sv.Type().Field(i).Name)
		field_name := sv.Type().Field(i).Name
		tag := sv.Type().Field(i).Tag
		if field_required, _ := strconv.ParseBool(tag.Get("jm_req")); field_required {
			if _, ok := j[field_name]; !ok {
				fmt.Println(field_name)
				probs = append(probs, field_name)
			}
		}
	}
	if len(probs) == 0 {
		return true, nil
	}
	fmt.Println(probs)
	return false, probs
}

/*
  NÃO FUNCIONAL !!!!!!!!!!!!!
  (Mas QUASE ...)
*/
func (j JSONMap) FillStruct(result interface{}) {
	t := reflect.ValueOf(result).Elem()
	for k, v := range j {
		fmt.Println(ustrings.UpperFirst(k))
		val := t.FieldByName(ustrings.UpperFirst(k))

		value_to_set := reflect.ValueOf(v)

		vt := reflect.TypeOf(v).Kind()
		fmt.Printf("- %s\n", vt)
		if vt == reflect.Float32 || vt == reflect.Float64 {
			fff, _ := reflect.ValueOf(value_to_set).Interface().(float64)
			fmt.Println(fff)
			vvvv := int64(fff)
			fmt.Println(vvvv)
			val.SetInt(vvvv)
		} else {
			val.Set(value_to_set)
		}
	}
}

func ToJSON(t interface{}) (s string) {
	b, err := json.Marshal(t)
	if err != nil {
		return ""
	}
	return string(b)
}

func ToJSONBytes(t interface{}) []byte {
	b, err := json.Marshal(t)
	if err != nil {
		return []byte("")
	}
	return b
}

func RequestToJSON(req *http.Request, jsonDoc interface{}) error {
	defer req.Body.Close()
	decoder := json.NewDecoder(req.Body)
	err := decoder.Decode(jsonDoc)
	if err != nil {
		return err
	}
	return nil
}
